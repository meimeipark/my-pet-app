import 'package:flutter/material.dart';

class PageIndex extends StatelessWidget {
  const PageIndex({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'SOONMOO 농장',
          style: TextStyle(
            fontWeight: FontWeight.w800,
            fontSize: 23,
          ),
        ),
        backgroundColor: Colors.orange,
        leading: Icon(Icons.menu),
        centerTitle: true,
        elevation: 0.0,
        actions: [
          IconButton(
              onPressed: () {},
              icon: Icon(Icons.person),
          ),
        ],
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            UserAccountsDrawerHeader(
              currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage('assets/soonmoo4.jpg'),
              ),
                accountName: Text('순무네 농장'),
                accountEmail: Text('soonmoo0won@soonmoo.com'),
                onDetailsPressed: () {},
                decoration: BoxDecoration(
                color: Colors.orange,
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(0, 30, 0, 0),
            ),
            Image.asset(
                'assets/soonmoo2.jpg',
              width: 500,
              height: 400,
              fit: BoxFit.fill,
            ),
            Container(
              child: Column(
                children: [
                  Text(
                      'SOONMOO를 소개합니다.',
                    style: TextStyle(
                      letterSpacing: 1.0,
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Container(
                    height: 1.0,
                    width: 350.0,
                    color: Colors.orange,
                    margin: EdgeInsets.fromLTRB(0, 10, 0, 25),
                  ),
                  Text(
                    '순무는 귀여워요. \n SNS 스타에요.',
                    style: TextStyle(
                      fontSize: 18,
                      letterSpacing: 1.2,
                    ),
                  ),
                ],
              ),
              padding: EdgeInsets.fromLTRB(30, 20, 30, 20),
            ),
            Container(
              child: Column(
                children: [
                  Row(
                    children: [
                      Container(
                        child: Image.asset(
                            'soonmoo5.jpg',
                          width: 200,
                          height: 200,
                          fit: BoxFit.fill,
                        ),
                        margin: EdgeInsets.fromLTRB(0, 0, 10, 10),
                      ),
                      Container(
                        child: Image.asset(
                            'soonmoo6.jpg',
                          width: 200,
                          height: 200,
                          fit: BoxFit.fill,
                        ),
                        margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Image.asset(
                            'soonmoo7.jpg',
                          width: 200,
                          height: 200,
                          fit: BoxFit.fill,
                        ),
                        margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                      ),
                      Container(
                        child: Image.asset(
                            'soonmoo8.jpg',
                          width: 200,
                          height: 200,
                          fit: BoxFit.fill,
                        ),
                        margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      ),
                    ],
                  ),
                ],
              ),
              margin: EdgeInsets.fromLTRB(135, 0, 135, 20),
            ),
          ],
        ),
      ),
    );
  }
}

